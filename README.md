# Tutoriel Django 

![Django](https://cdn-images-1.medium.com/max/1200/1*4QDbb-eO98wIB32CNNuWjw.png)

Dans le cadre de l'UE Veille Technologie, nous allons devoir développer une application de vente de livre en ligne à l'aide du framework Django.

## Introduction 

Dans la 1ère partie de ce tutoriel nous allons apprendre à installer et utiliser Django.

Django est un frameword Web Python qui a pour but de rendre la conception et le développement simple et rapide.

Django est gratuit et open source.

_"Le framework web pour les perfectionnistes sous pression"_

## Installation 
### Installation de Visual Studio Code

Pour commencer nous allons installer l'IDE ```Visual Studio Code```.

Vous pouvez le télécharger à cette adresse : 
[Télécharger Visual Studio Code](https://code.visualstudio.com/download).

### Installation de Python

Nous allons installer ```Python 3```.

Vous pouvez télécharger python à cette adresse : [Télécharger Python](https://www.python.org/downloads/).

### Installation de Pip

Nous allons aussi installer ```pip```, en effet pip est un gestionnaire de packages pour python.


Vous pouvez télécharger pip à cette adresse : [Télécharger Pip](http://apprendre-python.com/page-pip-installer-librairies-automatiquement).

### Installation de Virtualenv

```Virtualenv``` permet de créer des environnements virtuels Python isolés. Virtualenv crée un dossier contenant tous les exécutables nécessaires pour utiliser les paquets qu'un projet Python pourrait nécessiter.

Installer virtualenv via pip :

```
$ python3 -m pip install virtualenv
```

Si la commande précédente ne fonctionne pas nous allons exécuter l'installation via la version de python que vous avez télécharger.

Installer virtualenv via python : 

```
$ python3 -m pip install virtualenv
```

### Configuration de Virtualenv 

Nous allons créer un environnement de travail pour notre projet.

Pour cela nous allons commencez par créer un répertoire, tapez dans votre terminal la commande suivante : 

```
$ mkir mon_repertoire_de_projet
```

Rendez vous dans ce repertoire grâce à la commande suivante : 

```
$ cd ~/mon_repertoire_de_projet
```

Créer un environnement virtuel de projet nommé "myvenv" (vous pouvez le nommé comme vous le souhaitez) :

```
$ virtualenv myvenv
```

ou 

```
$ virtualenv -p python3 myvenv
```

Cette commande permet de créer un dossier qui contiendra les fichiers exécutables Python.

Pour commencer à utiliser l'environnement virtuel, il doit être activé : 

```
$ source myvenv/bin/activate
```

Dorénavant tous les paquets que nous installerons en utilisant pip seront placés dans le dossier `myvenv` isolé de l'installation globale de Python.

### Installation de Django

Nous y voilà enfin nous pouvons dès à présent installer ```Django``` en tapant la commande suivante :

```
$ pip install django==2.1.5
```

ou 

```
$ python3 -m pip install django==2.1.5
```


## Configuration

Nous venons d'installer toutes les dépendances nécessaires au fonctionnement de notre projet, nous pouvons maintenant créer notre projet et commencer la configuration.

### Génération du projet

Rendez vous dans le répertoire que nous avons créé précédemment avec la commande suivante :

```
$ cd ~/mon_repertoire_de_projet
$ django-admin startproject mysite
```
Cela va créer un répertoire ```mysite```dans le répertoire courant.

Nous allons créer notre application avec la commande : 

```
$ python manage.py startapp myapp
```

Cela va créer un répertoire ```myapp```dans le répertoire courant.

Nous avons maintenant dans notre répertoire de projet une structure de la façon suivante : 

> mon_ repertoire_ de_projet/
> 
> > myvenv
> > 
> > mysite/
> > > myapp/
> > > 
> > > mysite/
> > > 
> > > manage.py

### Configuration du projet 

Nous allons à présent configurer les réglages de notre projet.

Pour cela rendez vous dans le fichier *```mon_repertoire_de_projet/mysite/mysite/settings.py```*.

Ajoutez la ligne suivante :

```
INSTALLED_APPS = [
	...
	'myapp.apps.MyappConfig',
]
```

Rendez vous ensuite dans le fichier *```mon_repertoire_de_projet/mysite/mysite/urls.py```*.

```
from django.urls import path, include

urlpatterns = [
	path('myapp/', include('myapp.urls')),
]
```

### Configuration de l'application 

Nous allons à présent configurer notre application.

Rendez vous dans le fichier *```mon_repertoire_de_projet/mysite/myapp/view```* et ajoutez les lignes suivantes : 

```
from django.http import HttpResponse

def index(request):
	return HttpResponse("Hello, world. You're at the 'myapp' index.")

```

C’est la vue Django la plus simple possible. Pour appeler cette vue, il s’agit de l’associer à une URL. 
Nous allons donc créer un fichier nommé ```urls.py```.


Nous allons créer un fichier nommé ```urls.py ``` dans le dossier ```mon_repertoire_de_projet/mysite/myapp/```.

Rendez vous ensuite dans le fichier que nous venons de créer *```mon_repertoire_de_projet/mysite/myapp/urls.py```* et ajoutez-y les lignes suivantes : 

```
from django.urls import path
from .views import *

urlpatterns = [
	# serve index.html
	path('', index, name='index'),
]
```

### Installation de Pylint-Django

Nous allons installer pylint-django, c'est un plugin qui permet d'analyser et vérifier le code.

Pour l'installer il suffit de taper la commande suivante :

```
$ pip install pylint-django
```
ou 

```
$ python3 -m pip install pylint-django
```

Pour cette partie vous devrez ouvrir le projet dans l'IDE ```Visual Studio Code```.

Nous allons modifier le fichier ```setting.json``` dans le dossier ```.vscode```.

Insérez les lignes suivantes : 

```
{
	"python.pythonPath": "myvenv/bin/python",
	"python.linting.pylintArgs": [
		"--load-plugins=pylint_django"
	],
}
```
### Création de la base de données

Nous allons créer la base de données de notre application, pour cela entrez la commande suivante :  

```
$ python manage.py migrate
```

Nous allons créer un administrateur avec la commande suivante : 

```
$ python manage.py createsuperuser --username=admin --email=admin@example.com
```

### Lancement de l'application 

Nous allons à présent tester notre application avec la commande suivante : 

```
$ python manage.py runserver
```

Nous pouvons voir notre application à l'adresse [http://127.0.0.1:8000/myapp](http://127.0.0.1:8000/myapp) sur votre navigateur internet.

Nous pouvons nous connectez avec les identifiants de l'administrateur que vous avez créé précédemment ainsi que son mot de passe à l'adresse [http://127.0.0.1:8000/admin](http://127.0.0.1:8000/admin)

## Tutoriel API REST 

![Django API REST](https://www.django-rest-framework.org/img/logo.png)

## Introduction  

Dans la 2ème partie de ce tutoriel nous allons créer une API (Application Programming Interface) REST (Representational State Transfer) Django.

Le REST va permettre de nous orienter purement vers une API décrivant les ressources utilisées dans notre application Django. Le grand avantage d’utiliser une API REST est que nous avons une séparation entre Client et Serveur. 

Le grand avantage de REST est qu’il permet de requêter l’API avec de simples requêtes HTTP. 

 * Les requêtes GET vont permettre de récupérer des ressources 
 * Les requêtes POST vont permettre de créer des ressources 
 * Les requêtes DELETE vont permettre de supprimer des ressources 
 * Les requêtes PUT vont permettre de de mettre à jours des ressources


### Installation de Django REST Framework

Nous allons donc installer le framework permettant de créer notre API REST, utilisez la commande suivante pour l'installation : 

```
$ pip install djangorestframework
```

ou 

```
$ python3 -m pip install djangorestframework
```


### Configuration de Django REST Framework 

Nous allons à présent configurer les réglages de notre API.

Pour cela rendez vous dans le fichier *```mon_repertoire_de_projet/mysite/mysite/settings.py```*.

Ajoutez la ligne suivante :

```
INSTALLED_APPS = [
	...
	'rest_framework',
]
```

Rendez vous ensuite dans le fichier *```mon_repertoire_de_projet/mysite/myapp/urls.py```*.

Ajoutez les lignes suivantes : 

```
from rest_framework_swagger.views import get_swagger_view
from django.conf.urls import url

urlpatterns = [
	# [GET, POST books] + [GET, PUT, and DELETE book]
    path('books/', BookList.as_view(), name="book-list"),
    path('books/<int:pk>/', BookDetail.as_view(), name="book-details"),
]
```

### Création du modèle : Book

Nous allons créer notre premier modèle qui sera celui d'un livre, pour cela nous allons définir les attributs d'un livre.

Rendez vous dans le fichier *```mon_repertoire_de_projet/mysite/myapp/models.py```*

Ajoutez les lignes suivantes : 

```
class Book(models.Model):
    title          = models.CharField(max_length = 256, null = True, blank = True)
    author         = models.CharField(max_length = 256, null = True, blank = True)
    image          = models.CharField(max_length = 256, null = True, blank = True)
    year           = models.PositiveIntegerField(default = 0, null = True, blank = True)
    price          = models.FloatField(default = -1.0, null = True, blank = True)
    description    = models.TextField(max_length = 256, null = True, blank = True)
    bestseller     = models.BooleanField(default = False)
```

### Création du Serializers

Les serializers vont permettre de serialiser les instances en JSON et transformer le JSON en instance python.

Nous allons créer un fichier nommé ```serializers.py ``` dans le dossier ```mon_repertoire_de_projet/mysite/myapp/```.

Rendez vous ensuite dans le fichier que nous venons de créer *```mon_repertoire_de_projet/mysite/myapp/serializer.py```* et ajoutez-y les lignes suivantes : 

```
from rest_framework import serializers
from .models import Book

class BookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = '__all__'
        #field = ('title', 'author', ... etc)
```

### Création de la view

Nous allons nous rendre dans le fichier *```mon_repertoire_de_projet/mysite/myapp/view.py```*.

Nous allons ajoutez les imports suivant : 

```
from django.http import HttpResponse, JsonResponse, Http404

from rest_framework.views import APIView
from rest_framework import status

from .models import Book
from .serializers import BookSerializer
```

### View : La Classe BookList

Toujours dans le fichier ```view.py```, ajoutez les lignes de code suivantes pour créer la classe ```BookList```.

```
class BookList(APIView):
    """
    List all books, or create a new book.
    """

    def get(self, request, format = None):
        """
        List all books.
        """
        books = Book.objects.all()
        serializer = BookSerializer(books, many = True)
        return JsonResponse(serializer.data, safe = False)

    def post(self, request, format = None):
        """
        Create a new book.
        """
        serializer = BookSerializer(data = request.data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, safe = False, status = status.HTTP_201_CREATED)
        return JsonResponse(serializer.errors, safe = False, status = status.HTTP_400_BAD_REQUEST)
```

Cette classe possède les méthodes GET et POST qui vont permettre de retrouver ou créer un livre.


### View : La classe BookDetail 

Toujours dans le fichier ```view.py```, ajoutez les lignes de code suivantes pour créer la classe ```BookDetail```.

```
class BookDetail(APIView):
    """
    Retrieve, update or delete a book instance.
    """

    def get_object(self, pk):
        try:
            return Book.objects.get(pk = pk)
        except Book.DoesNotExist:
            raise Http404

    def get(self, request, pk, format = None):
        """
        Retrieve a book.
        """
        book = self.get_object(pk)
        serializer = BookSerializer(book)
        return JsonResponse(serializer.data, safe = False)

    def put(self, request, pk, format = None):
        """
        Update a book.
        """
        book = self.get_object(pk)
        serializer = BookSerializer(book, data = request.data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, safe = False)
        return JsonResponse(serializer.errors, safe = False, status = status.HTTP_404_NOT_FOUND)

    def delete(self, request, pk, format = None):
        """
        Delete a book.
        """
        book = self.get_object(pk)
        book.delete()
        return JsonResponse({}, safe = False, status = status.HTTP_204_NO_CONTENT)
```

Cette classe possède les méthodes GET, PUT et DELETE qui vont permet de retrouver, éditer ou supprimer un livre.

### Ajout de la section Book dans la partie administration

Finalement rendez vous dans le fichier *```mon_repertoire_de_projet/mysite/myapp/admin.py```* et rajoutez ces lignes :

```
from .models import Book

admin.site.register(Book)
```

Cela va permettre de créer, éditer ou supprimer un livre directement depuis l'écran de gestion de notre application.

### Migration de la base de Données 

À chaque modification du modèle ou de la base de données, nous devons faire une migration de la base de données.

Donc rendez vous dans le terminal puis taper les commandes suivantes :

```
$ python manage.py makemigrations myapp
$ python manage.py migrate 
```

### Lancement de l'application 

```
$ python manage.py runserver
```

Nous pouvons voir la liste des livres de notre application à l'adresse [http://127.0.0.1:8000/myapp/books](http://127.0.0.1:8000/myapp/books)

Nous pouvons rajoutez des livres à cette adresse [http://127.0.0.1:8000/myapp/admin](http://127.0.0.1:8000/myapp/admin)
dans la section ```Book```

## Swagger 

![Swagger](https://www.developpez.net/forums/attachment.php?attachmentid=331436&d=1512059512)

Afin d'utiliser facilement notre API, il est important d’y apporter de la documentation.

Pour cela, nous allons installer ```Swagger``` qui est un générateur automatique de documentation. Il va permettre de montrer les urls disponibles pour les requêtes HTTP, mais aussi le type de retour de chaque champ de notre JSON, etc.

Commençons par l'installation du framework : 

```
$ pip install rest_framework_swagger==2.1.0
```
ou 

```
$ python3 -m pip install rest_framework_swagger==2.1.0
```

Ensuite on va ajouter le **Swagger** à notre liste d'application **Django** dans le fichier *```settings.py```*

Pour cela rendez vous dans le fichier *```mon_repertoire_de_projet/mysite/mysite/settings.py```*.

Ajoutez la ligne suivante :

```
INSTALLED_APPS = [
	...
	'rest_framework_swagger',
]
```

Pour finir nous allons inclure l'url de notre **Swagger** dans le fichier *```urls.py```*

Pour cela rendez vous dans le fichier *```mon_repertoire_de_projet/mysite/mysite/urls.py```* et ajoutez les lignes suivantes : 

```
from rest_framework_swagger.views import get_swagger_view
from django.conf.urls import url

schema_view = get_swagger_view(title='Django-Tutoriel')

urlpatterns = [
	...
	url('swagger/', schema_view),
]
```

Vous pouvez retrouver le résultat à l'adresse suivante : [http://127.0.0.1:8000/myapp/swagger/](http://127.0.0.1:8000/myapp/swagger/)


## PostgreSQL

![PostgreSQL](https://www.postgresql.org/media/img/about/press/slonik_with_black_text_and_tagline.gif)

Par défaut Django crée une base de données SQLite, nous allons changer la base de donnée de notre application par une base de données PostgreSQL.

### Installation de PostgreSQL

Nous allons installer ```PostgreSQL```.

Vous pouvez télécharger PostgreSQL à cette adresse : [Télécharger PostgreSQL](https://postgresapp.com).

Nous allons aussi installer ```Psycopg2``` qui est un adaptateur de la base de données PostgreSQL pour le langage de programmation **Python**.

Lancer l'application PostgreSQL 11, appuyer sur le bouton ```initialize``` pour lancer un nouveau serveur.

### Création de la base de données

Cliquez sur start pour commencer la création de la base de données, vous serez rediriger vers un nouveau terminal avec comme particularité d'être dans un terminal SQL, ce terminal permet de gérer la base de données. 

**(Dans ce terminal SQL, chaque commande doit impérativement se terminer par ; )**

Nous allons créer la base de données de notre application, pour cela entrez la commande suivante :

```
$ CREATE DATABASE myproject ;
```

Nous allons ensuite créer un utilisateur de notre base de données, entrez la commande suivante : 

```
$ CREATE USER myprojectuser WITH PASSWORD 'password' ;
```

Nous allons faire quelques réglages pour la base de données, entrez les commandes suivantes : 

```
$ ALTER ROLE myprojectuser SET client_encoding TO 'utf8' ;
$ ALTER ROLE myprojectuser SET default_transaction_isolation TO 'read committed' ;
$ ALTER ROLE myprojectuser SET timezone TO 'UTC' ;
```

Finalement nous allons ensuite donnée les accès à la base de données pour l'utilisateur que nous venons de créer : 

```
$ GRANT ALL PRIVILEGES ON DATABASE myproject TO myprojectuser ;
```

Voilà tous les réglages de notre base de données sont finis, nous pouvons à présent quitter le terminal avec la commande suivante :

```
$ \q
```
ou

```
$ exit 
```

### Configuration de Django avec la base de données 

Nous allons maintenant relier notre base de données à notre application dans le fichier ```settings.py```.

Pour cela rendez vous dans le fichier *```mon_repertoire_de_projet/mysite/mysite/settings.py```*.


```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'myproject',
        'USER': 'myprojectuser',
        'PASSWORD': 'password',
        'HOST': 'localhost',
        'PORT': '',
    }
}
```

### Migration de la base de données 

Maintenant que les réglages de **Django** sont finis nous allons pouvoir migrés la base de données vers notre nouvelle base de données.

Tapez les commandes suivantes : 

```
$ python manage.py makemigrations
$ python manage.py migrate
```

Après avoir créé toutes la structure de notre base de données, nous allons à présent créer un administrateur, pour cela entrez la commande suivante : 

```
$ python manage.py createsuperuser
```

Vous devrez entrez un identifiant, une adresse mail ainsi qu'un mot de passe pour accéder à la partie administration de l'application.

### Lancement de la base de données 

Comme précédemment, vous pouvez voir notre application à l'adresse [http://127.0.0.1:8000/myapp](http://127.0.0.1:8000/myapp) sur votre navigateur internet.

Vous pouvez vous connectez avec les identifiants de l'administrateur que vous avez créé précédemment ainsi que le mot de passe à l'adresse [http://127.0.0.1:8000/admin](http://127.0.0.1:8000/admin)



## JSON Web Tokens 

![JWT](https://cdn-images-1.medium.com/max/1600/1*XkmnsJ6Joa6EDFVGUw0tfA.png)

### Installation de JWT 

Nous allons à présent installer ```JWT```, qui va nous permettre de générer un token pour assurer la sécurité nos requêtes.

Pour installer ```JWT``` tapez la commande suivante : 

```
$ pip install djangorestframework_simplejwt==3.3
```

ou 

```
$ python3 -m pip install djangorestframework_simplejwt==3.3
```

### Configuration de JWT

Nous allons configurer notre application avec JWT.

Pour cela rendez vous dans le fichier *```mon_repertoire_de_projet/mysite/mysite/settings.py```*

Entrez ces lignes de commandes :

```
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework_simplejwt.authentication.JWTAuthentication',
    ],
}

SWAGGER_SETTINGS = {
    'SECURITY_DEFINITIONS': {
        'api_key': {
            'type': 'apiKey',
            'in': 'header',
            'name': 'Authorization'
        }
    }
}
```

Rendez vous ensuite dans le fichier *```mon_repertoire_de_projet/mysite/myapp/urls.py```* et rajoutez les lignes de code suivante : 

```
from rest_framework_simplejwt import views as jwt_views

urlpatterns = [
    ...
    path('api/token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
]
```
Puis dans le fichier *```mon_repertoire_de_projet/mysite/myapp/view.py```* et rajoutez les lignes de code suivante : 

```
from rest_framework.permissions import IsAuthenticated
```

Ainsi que dans nos deux classes ```BookDetail``` et ```BookList```, rajoutez :

```
permission_classes = (IsAuthenticated,)
```

Maintenant nous allons appliquer ces changements à la base de données, avec les deux commandes suivantes : 

```
$ python manage.py makemigrations
$ python manage.py migrate
```


Lancez le serveur pour générer nos tokens.

```
$ python manage.py runserver 
```

### Génération des tokens

Pour générer le token nous allons nous rendre à l'adresse suivante : 
[http://127.0.0.1:8000/myapp/api/token/](http://127.0.0.1:8000/myapp/api/token/)

Saisissez un username ainsi qu'un mot de passe que nous avons définie plus tôt, puis cliquez sur POST.

Un résultat avec 2 tokens doivent être générer sous cette forme : 

```
HTTP 200 OK
Allow: POST, OPTIONS
Content-Type: application/json
Vary: Accept

{
		"refresh": "***",
		"access": "***"
	}

```

Copier le token "access" généré aléatoirement, puis rendez vous dans le **Swagger** à cette adresse : [http://127.0.0.1:8000/myapp/swagger/](http://127.0.0.1:8000/myapp/swagger/)

Vous pouvez constater que la section ```Books``` a disparu et qu'une nouvelle section ```API``` est apparue. 

Cette section permet de générer les token comme vue précédemment en entrant l'username et le mot de passe.

Entrez le token générer dans l'onglet ```Authorize``` (en haut à droite) sous la forme suivante : 

```
Bearer <token access>
```

Maintenant la section ```Books``` est revenu et vous pouvez tester vos requêtes.

Ce token n'est valable que pendant une durée de 5 minutes, au-delà vous devrez re générer un token en utilisant le "refresh".

Pour cela rendez vous à cette adresse : [http://127.0.0.1:8000/myapp/api/token/refresh/](http://127.0.0.1:8000/myapp/api/token/refresh/)

Entrez le token "refresh" que vous avez généré précédemment, cela vous donnera à nouveau un token "access". Puis renouveler la manipulation dans le **Swagger**.


## PostMan 

![PostMan](https://cdn-images-1.medium.com/max/1600/1*QOx_tPV5wJnhTzAGhfIiLA.png)

### Testons notre API à Postman

Postman est un logiciel permettant de tester facilement les requêtes HTTP, il se focalise sur les tests d'API. Il permet d'envoyer toutes sorte de requêtes et de les personnaliser mais aussi de gérer les authentifications, les scripts de test, etc.

Vous pouvez télécharger le logiciel à cette adresse : [Télécharger Postman](https://www.getpostman.com).

![Postman](https://image.noelshack.com/fichiers/2019/06/1/1549314971-postman.png)

Voici les principales fonctionnalités à connaitre pour les tests :

* (1) : Choix du type de requête (GET, POST, PUT, DELETE, etc.)
* (2) : Url de l'API 
* (3) : Les paramètres à passer dans l'Url
* (4) : Tout ce qui constitue une requête HTTP (Header, Body, etc.)

### Test un POST 

Pour faire un test sur un POST, sélectionnez POST puis ajoutez l'url, cliquez sur l'onglet Body puis sélectionnez Raw pour définir manuellement le contenu de la requête HTTP, enfin dans l'onglet le plus à droite sélectionnez JSON (application/json). Et pour finalisez le test entrez un JSON avec les informations et appuyez sur le bouton "Send".

Rien ne se passe ? C'est Ok, mais nous pouvons constatez une réponse avec le code 201. 

Pour vérifier que notre POST a bien marché, il suffit d'effectuer un GET pour constater l'ajout.

### Test un GET 

Pour faire un test sur un GET, il suffit simplement de choisir le type de requête, sélectionnez GET puis de rentrer l'url et de valider avec le bouton "Send".

## Bonus 

###  Requirements.txt 

Nous allons à présent généré un fichier .txt qui va nous permettre de savoir les dépendances obligatoires de notre projet.

Rendez vous dans votre environnement virtuel avec les commandes suivantes :

```
$ cd ~/mon_repertoire_de_projet
$ source myvenv/bin/activate
```

Ensuite généré le fichier en tapant la commande suivante : 

```
$ pip freeze > requirements.txt
```
ou

```
$ python3 -m pip freeze > requirements.txt
```

### Quelques commandes git 

Nous allons voir les commandes principales à utiliser pour mener à bien ce projet.

Tout d'abord rendez vous dans votre répertoire avec la commande suivante : 

```
$ cd ~/mon_repertoire_de_projet
```

Pour relier notre répertoire avec un repositorie Git, tapez les commandes suivantes : 


```
$ git init 
$ git remote add origin <lien de votre repositorie>
```

Chaque fois que vous créez/modifiez un fichier sur votre machine vous devrez l'ajoutez à votre repositorie.

Pour cela voici les commandes principales.

* **Permet de voir les nouveaux fichier/modification pas encore envoyez sur le repositorie.**

```
$ git status
```

* **Permet d'ajouter tous les nouveaux fichier/modification.**

```
$ git add .
```

* **Permet d'envoyer sur le serveur local vos modifications.**

```
$ git commit -m "message explicatif du commit"
```

* **Permet d'envoyer sur le serveur distant vos modifications.**

```
$ git push 
```

* **Permet de récupérer les fichiers sur le serveur distant.**

```
$ git pull
```

Lors de votre 1er envoi sur le serveur distant, vous devrez communiquez vos identifiant ainsi que votre mot de passe.


### .gitignore 

Nous allons à présent créer un fichier .gitignore qui va permettre d'ignorer certains fichiers.

Cela permet par exemple de ne pas envoyer sur le repositorie en ligne certain fichier contenant des mots de passe ou configurations, ou encore des fichiers générés automatiquement par certain IDE ou machine.

Dans **Visual Studio Code**, créer un nouveau fichier que vous nommerez ```.gitignore``` et vous allez ajouter les lignes de code du lien suivant : [gitignore](https://github.com/jpadilla/django-project-template/blob/master/.gitignore).

Maintenant tous vos prochains commit ne contiendront plus les fichiers ajoutés à votre gitignore.

Si vous avez un fichier sur votre repositorie que vous voulez retirer car vous avez décidez de le mettre dans votre ```.gitignore```, tapez les commandes suivantes : 

```
$ git rm --cached <le nom du fichier à retirer>
$ git commit -m "retrait du fichier en question"
$ git push
```

Voilà votre fichier ne fait plus partie du repositorie.


***Par Halfon Nessim***
